import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.worklog.WorklogImpl;

def user = ComponentAccessor.jiraAuthenticationContext.getLoggedInUser()
def issueManager = ComponentAccessor.getIssueManager()
def watcherManager = ComponentAccessor.getWatcherManager()
def worklogManager = ComponentAccessor.getWorklogManager()
    
def issues = issueManager.getWatchedIssues(user)
issues.each { issue ->
    def watchers = watcherManager.getWatchersUnsorted(issue)
    watchers.each {watcher ->
        if (watcher != user) {
    		watcherManager.stopWatching(watcher, issue)	
        }
    }
    def worklog = new WorklogImpl(worklogManager, issue, 0L, (String)issue.reporter.name, issue.summary, new Date(), null, null, 1*3600L)
    worklogManager.create(user, worklog, 0L, false)
}

